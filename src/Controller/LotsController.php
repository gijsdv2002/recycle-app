<?php

namespace App\Controller;

use App\Entity\Lots;
use App\Form\LotsType;
use App\Repository\LotsRepository;
use App\Repository\UserRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/")
 */
class LotsController extends AbstractController
{
//    renders main page
    /**
     * @Route("/", name="lots_index", methods={"GET"})
     */
    public function index(LotsRepository $lotsRepository): Response
    {
        return $this->render('lots/index.html.twig', [
            'lots' => $lotsRepository->findBy(['sold' => 0]),
        ]);
    }
//makes form and submit it
    /**
     * @Route("/new", name="lots_new", methods={"GET","POST"})
     */
    public function new(Request $request, SluggerInterface $slugger): Response
    {
        $lot = new Lots();
        $form = $this->createForm(LotsType::class, $lot);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $lotImgFile = $form->get('image')->getData();

            $originalFilename = pathinfo($lotImgFile->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $lotImgFile->guessExtension();
            $lotImgFile->move(
                $this->getParameter('photo_directory'),
                $newFilename
            );
            $lot->setImage($this->getParameter('photo_directory') . $newFilename);
            $lot->setSold(0);
            $lot->setSellerId($this->getUser());
            $lot->setCreatedAt(new DateTime());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($lot);
            $entityManager->flush();

            return $this->redirectToRoute('lots_index');
        }

        return $this->render('lots/new.html.twig', [
            'lot' => $lot,
            'form' => $form->createView(),
        ]);
    }

//removes lots if user requests
    /**
     * @Route("/{id}", name="lots_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Lots $lot): Response
    {
        if ($this->isCsrfTokenValid('delete' . $lot->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($lot);
            $entityManager->flush();
        }

        return $this->redirectToRoute('lots_index');
    }
// makes it possible to buy lots
    /**
     * @Route("/buy/{id}", name="lots_buy", methods={"GET","POST"})
     */
    public function buy(Request $request, Lots $lot, UserRepository $userRepository): Response
    {

        if ($this->getUser()->getMoney() >= $lot->getPrice()) {
            $this->getUser()->setMoney($this->getUser()->getMoney() - $lot->getPrice());
            $getMoney = $userRepository->findOneBy(['id' => $lot->getSellerId()->getId()]);
            $getMoney->setMoney($getMoney->getMoney() + $lot->getPrice());
            $lot->setSold(1);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($lot);
            $entityManager->persist($getMoney);
            $entityManager->flush();
            $this->addFlash('succes', 'het product is gekocht!');
            return $this->redirectToRoute('lots_index');

        }
        $this->addFlash('notice', 'te weinig geld!');
        return $this->redirectToRoute('lots_index');

    }
}
