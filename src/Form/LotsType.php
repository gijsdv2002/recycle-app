<?php

namespace App\Form;

use App\Entity\Lots;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class LotsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image', FileType::class, [
                'required' => true,
                'label' => 'foto\'s ',
                'mapped' => 'false',
                'constraints' => [
                    new File([
                        'maxSize' => '204800k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png'
                        ]
                        ,
                        'mimeTypesMessage' => 'Voer een geldig foto type in!',

                    ])
                ]
            ])
            ->add('title', TextType::class, ['label' => 'titel', 'required' => true,])
            ->add('discription', TextareaType::class, ['label' => 'beschrijving', 'required' => true])
            ->add('price', IntegerType::class, ['label' => 'prijs', 'required' => true,]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lots::class,
        ]);
    }
}
